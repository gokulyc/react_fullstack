- yarn create react-app hackernews-react-apollo
- yarn start
- yarn add @apollo/client graphql

- curl https://codeload.github.com/howtographql/react-apollo/tar.gz/starter | tar -xz --strip=1 react-apollo-starter/server

#### install the dependencies
- cd server
- yarn

#### First, we need to save an initial migration.
- cd server
- npx prisma migrate save --experimental  
\> Supply a migration name

#### we need to run the migration.
- npx prisma migrate up --experimental

#### Finally, we generate the Prisma Client.
- npx prisma generate

### Navigate into the server directory and run the following commands to start the server(graphql) :

- yarn dev

### Why are there two GraphQL API layers in a backend architecture with Prisma?  
> Prisma provides the database layer which offers CRUD operations. The second layer is the application layer for business logic and common workflows (like authentication).

### What's the declarative way for loading data with React & Apollo?
> Using the '<Query />' component and passing GraphQL query as prop

1. >'<Mutation />' component allow variables, optimisticResponse, refetchQueries, and update as props

- yarn add react-router react-router-dom

Description: The best learning resource for GraphQL  
URL: howtographql.com  

### What's the purpose of the 'withApollo' function?
> When wrapped around a component, it injects the 'ApolloClient' instance into the component's props